package com.target.targetcasestudy

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.GeneralLocation
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.target.targetcasestudy.screens.ItemScreen
import com.target.targetcasestudy.screens.ListScreen
import com.target.targetcasestudy.screens.MainScreen
import org.junit.Rule
import org.junit.Test

class AppEndToEndTest: TestCase() {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testApp() {
        run {
            step("Open the app and check state") {
                MainScreen {
                    toolbar.isDisplayed()
                    toolbar.hasTitle(R.string.list)
                    frameLayout.isDisplayed()
                }
            }

            step("Check the recycler view state and scroll and click") {
                ListScreen {
                    recyclerView {
                        flakySafely {
                            isDisplayed()
                            Espresso.onView(ViewMatchers.withId(R.id.deals_recycler_view))
                                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(10, ViewActions.click()))
                        }
                    }
                }
            }
        }


        run {
            step("Check the detail screen state") {
                ItemScreen {
                    checkDealScreenState()
                    scrollView.scrollToEnd()
                    description.isDisplayed()
                    addToCartButton {
                        isDisplayed()
                        isEnabled()
                        click(GeneralLocation.CENTER)
                    }
                    pressBack()
                }
            }
            step("Check that we go back to the list screen") {
                ListScreen {
                    recyclerView.isDisplayed()
                }
            }
        }
    }
}
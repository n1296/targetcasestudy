package com.target.targetcasestudy.screens

import com.kaspersky.kaspresso.screens.KScreen
import com.target.targetcasestudy.R
import com.target.targetcasestudy.ui.DealListFragment
import io.github.kakaocup.kakao.recycler.KRecyclerView

object ListScreen: KScreen<ListScreen>() {
    override val layoutId: Int = R.layout.fragment_deal_list
    override val viewClass: Class<*> = DealListFragment::class.java

    val recyclerView = KRecyclerView(builder = { withId(R.id.deals_recycler_view) }, {})
}
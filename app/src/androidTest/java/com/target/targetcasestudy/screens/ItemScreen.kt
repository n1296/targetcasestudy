package com.target.targetcasestudy.screens

import com.kaspersky.kaspresso.screens.KScreen
import com.target.targetcasestudy.R
import com.target.targetcasestudy.ui.DealItemFragment
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.scroll.KScrollView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

object ItemScreen: KScreen<ItemScreen>() {
    override val layoutId: Int = R.layout.fragment_deal_item
    override val viewClass: Class<*> = DealItemFragment::class.java

    private val image = KImageView {
        withId(R.id.deal_item_image)
    }

    private val title = KTextView {
        withId(R.id.deal_item_title)
    }

    private val salePrice = KTextView {
        withId(R.id.deal_item_price)
    }

    private val regularPrice = KTextView {
        withId(R.id.deal_item_regular_price)
    }

    private val fulfillment = KTextView {
        withId(R.id.deal_item_fulfillment)
    }

    private val productDetails = KTextView {
        withText(R.string.product_details)
    }

    val description = KTextView {
        withId(R.id.detail_item_description)
    }

    val scrollView = KScrollView {
        withId(R.id.deal_scroll_view)
    }

    val addToCartButton = KButton {
        withId(R.id.add_to_cart_button)
    }

    fun checkDealScreenState() {
        image.isDisplayed()
        title.isDisplayed()
        title.hasAnyText()
        salePrice.isDisplayed()
        salePrice.hasAnyText()
        regularPrice.isDisplayed()
        regularPrice.hasAnyText()
        fulfillment.hasAnyText()
        fulfillment.isDisplayed()
        productDetails.isDisplayed()
        description.isDisplayed()
        description.hasAnyText()
        addToCartButton.isDisplayed()
        addToCartButton.hasText(R.string.add_to_cart)
        scrollView.isDisplayed()
    }
}
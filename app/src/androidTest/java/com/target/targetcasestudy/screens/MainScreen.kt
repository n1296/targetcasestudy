package com.target.targetcasestudy.screens

import com.kaspersky.kaspresso.screens.KScreen
import com.target.targetcasestudy.MainActivity
import com.target.targetcasestudy.R
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.toolbar.KToolbar

object MainScreen: KScreen<MainScreen>() {
    override val layoutId: Int = R.layout.activity_main
    override val viewClass: Class<*> = MainActivity::class.java

    val toolbar = KToolbar {
        withId(R.id.target_toolbar)
    }

    val frameLayout = KView {
        withId(R.id.container)
    }
}
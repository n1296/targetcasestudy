package com.target.targetcasestudy.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.target.targetcasestudy.api.Deal
import com.target.targetcasestudy.api.DealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DealListViewModel @Inject constructor(
    private val dealRepository: DealRepository
): ViewModel() {
    private val _dealListLiveData = MutableLiveData<List<Deal>?>()
    val dealListLiveData: LiveData<List<Deal>?> = _dealListLiveData

    fun fetchDeals() {
        viewModelScope.launch(Dispatchers.IO) {
            val dealsFromResponse = dealRepository.getDeals()?.deals
            _dealListLiveData.postValue(dealsFromResponse)
        }
    }
}

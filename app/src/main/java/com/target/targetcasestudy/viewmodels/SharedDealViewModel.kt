package com.target.targetcasestudy.viewmodels

import androidx.lifecycle.ViewModel
import com.target.targetcasestudy.api.DealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

data class ViewModelState(
    val currentDealId: String?
)

@HiltViewModel
class SharedDealViewModel @Inject constructor(
    private val dealRepository: DealRepository
): ViewModel() {
    var state = ViewModelState(null)
}
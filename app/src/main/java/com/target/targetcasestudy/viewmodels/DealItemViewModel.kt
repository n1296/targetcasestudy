package com.target.targetcasestudy.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.target.targetcasestudy.api.Deal
import com.target.targetcasestudy.api.DealRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DealItemViewModel @Inject constructor(
    private val dealRepository: DealRepository
): ViewModel() {

    private val _dealLiveData = MutableLiveData<Deal?>()
    val dealLiveData: LiveData<Deal?> = _dealLiveData

    fun fetchDeal(dealId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val dealResponse = dealRepository.getDeal(dealId)
            _dealLiveData.postValue(dealResponse)
        }
    }
}

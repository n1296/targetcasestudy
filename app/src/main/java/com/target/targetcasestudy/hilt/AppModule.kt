package com.target.targetcasestudy.hilt

import com.target.targetcasestudy.api.DealApiKtx
import com.target.targetcasestudy.api.DealRepository
import com.target.targetcasestudy.api.RetrofitAPIBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideApiKtx(): DealApiKtx = RetrofitAPIBuilder.apiService

    @Provides
    @Singleton
    fun provideDealRepository(dealApi: DealApiKtx): DealRepository = DealRepository(dealApi)
}
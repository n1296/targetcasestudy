package com.target.targetcasestudy.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.target.targetcasestudy.R
import com.target.targetcasestudy.api.Deal

class DealItemAdapter : RecyclerView.Adapter<DealItemViewHolder>() {

    private val _dealItemClicked = MutableLiveData<String>()
    val dealItemClicked: LiveData<String> = _dealItemClicked

    var dealsList: List<Deal> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.deal_list_item, parent, false)
        return DealItemViewHolder(view)
    }

    override fun getItemCount(): Int = dealsList.count()

    override fun onBindViewHolder(viewHolder: DealItemViewHolder, position: Int) {
        val item = dealsList[position]
        viewHolder.apply {
            //TODO account for price localization
            salePrice.text = item.salePrice?.displayString ?: item.regularPrice.displayString
            fulfillment.text = item.fulfillment
            title.text = item.title
            availability.text = item.availability
            aisle.text = aisle.context.getString(R.string.aisle, item.aisle)
            regularPrice.text =
                regularPrice.context.getString(R.string.regular_price, item.regularPrice.displayString)

            Glide.with(image)
                .asBitmap()
                .load(item.imageUrl)
                .fitCenter()
                .placeholder(R.drawable.ic_launcher_background)
                .into(image)

            itemView.setOnClickListener {
                _dealItemClicked.postValue(item.id)
            }
        }
    }
}

class DealItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val image: ImageView = itemView.findViewById(R.id.deal_list_item_image_view)
    val salePrice: TextView = itemView.findViewById(R.id.deal_list_item_price)
    val fulfillment: TextView = itemView.findViewById(R.id.deal_list_item_fulfillment)
    val title: TextView = itemView.findViewById(R.id.deal_list_item_title)
    val availability: TextView = itemView.findViewById(R.id.deal_list_item_stock_availability)
    val aisle: TextView = itemView.findViewById(R.id.deal_list_item_aisle)
    val regularPrice: TextView = itemView.findViewById(R.id.deal_list_item_regular_price)

}
package com.target.targetcasestudy.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.target.targetcasestudy.MainActivity
import com.target.targetcasestudy.R
import com.target.targetcasestudy.viewmodels.DealListViewModel
import com.target.targetcasestudy.viewmodels.SharedDealViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DealListFragment : Fragment() {

    private val sharedViewModel: SharedDealViewModel by lazy {
        ViewModelProvider(requireActivity())[SharedDealViewModel::class.java]
    }

    private val viewModel: DealListViewModel by lazy {
        ViewModelProvider(this)[DealListViewModel::class.java]
    }

    private var dealAdapter: DealItemAdapter = DealItemAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchDeals()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_deal_list, container, false)

        (requireActivity() as MainActivity).supportActionBar?.title =
            requireContext().getString(R.string.list)

        (requireActivity() as MainActivity).
            supportActionBar?.setDisplayHomeAsUpEnabled(false)

        view.findViewById<RecyclerView>(R.id.deals_recycler_view).apply {
            layoutManager = LinearLayoutManager(requireContext())
            val itemDecoration =
                DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL)
            ResourcesCompat.getDrawable(resources, R.drawable.divider, null)?.let { divider ->
                itemDecoration.setDrawable(divider)
            }
            addItemDecoration(itemDecoration)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeDeals()
        observeDealClicks()
    }

    private fun observeDeals() {
        // todo add progress bar or handle loading more elegantly/animation
        viewModel.dealListLiveData.observe(viewLifecycleOwner) { deals ->
            deals?.let {
                dealAdapter.dealsList = deals
                view?.findViewById<RecyclerView>(R.id.deals_recycler_view)?.adapter = dealAdapter
            } ?: Toast.makeText(requireContext(), getString(R.string.error_message), Toast.LENGTH_SHORT).show()
        }
    }

    private fun observeDealClicks() {
        dealAdapter.dealItemClicked.observe(viewLifecycleOwner) { id ->
            sharedViewModel.state = sharedViewModel.state.copy(currentDealId = id)
            // todo update to nav graph
            parentFragmentManager
                .beginTransaction()
                .add(R.id.container, DealItemFragment.newInstance())
                .addToBackStack(null)
                .commit()
        }
    }

    companion object {
        fun newInstance(): DealListFragment = DealListFragment()
    }
}

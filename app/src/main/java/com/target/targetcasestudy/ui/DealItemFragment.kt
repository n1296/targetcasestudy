package com.target.targetcasestudy.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.target.targetcasestudy.MainActivity
import com.target.targetcasestudy.R
import com.target.targetcasestudy.viewmodels.DealItemViewModel
import com.target.targetcasestudy.viewmodels.SharedDealViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DealItemFragment : Fragment() {
    private val sharedViewModel: SharedDealViewModel by lazy {
        ViewModelProvider(requireActivity())[SharedDealViewModel::class.java]
    }

    private val viewModel: DealItemViewModel by lazy {
        ViewModelProvider(this)[DealItemViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel.state.currentDealId?.let { id ->
            viewModel.fetchDeal(id)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity() as MainActivity).supportActionBar?.title =
            requireContext().getString(R.string.details)
        (requireActivity() as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deal_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("neal","on view created")

        val image = view.findViewById<ImageView>(R.id.deal_item_image)
        val title = view.findViewById<TextView>(R.id.deal_item_title)
        val description = view.findViewById<TextView>(R.id.detail_item_description)
        val regularPrice = view.findViewById<TextView>(R.id.deal_item_regular_price)
        val salePrice = view.findViewById<TextView>(R.id.deal_item_price)
        val fulfillment = view.findViewById<TextView>(R.id.deal_item_fulfillment)
        val addToCardButton = view.findViewById<AppCompatButton>(R.id.add_to_cart_button)

        addToCardButton.setOnClickListener {
            Toast.makeText(requireContext(), getString(R.string.add_to_cart_toast), Toast.LENGTH_SHORT).show()
        }

        viewModel.dealLiveData.observe(viewLifecycleOwner) { deal ->
            Log.d("neal loaded", "loaded ${deal?.title}")
            deal?.let {
                title.text = deal.title
                regularPrice.text =
                    requireContext().getString(
                        R.string.regular_price,
                        deal.regularPrice.displayString
                    )
                salePrice.text = deal.salePrice?.displayString ?: deal.regularPrice.displayString
                fulfillment.text = deal.fulfillment
                description.text = deal.description
                Glide.with(image)
                    .asBitmap()
                    .load(deal.imageUrl)
                    .fitCenter()
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(image)
            } ?: Toast.makeText(
                requireContext(),
                getString(R.string.error_message),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.dealLiveData.removeObservers(viewLifecycleOwner)
    }

    companion object {
        fun newInstance(): DealItemFragment {
            return DealItemFragment()
        }
    }
}

package com.target.targetcasestudy.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


object RetrofitAPIBuilder {
    val apiService: DealApiKtx = retrofit.create(DealApiKtx::class.java)

    private val moshi: Moshi
        get() = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()


    private val retrofit: Retrofit
        get() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
}
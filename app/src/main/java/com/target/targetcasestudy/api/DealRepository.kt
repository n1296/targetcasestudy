package com.target.targetcasestudy.api

import javax.inject.Inject

class DealRepository @Inject constructor(
    private val apiService: DealApiKtx
) {

    // TODO add more elegant error handling
    suspend fun getDeals(): DealResponse? {
        val response = apiService.retrieveDeals()
        return if (response.isSuccessful) {
            response.body()
        } else {
            null
        }
    }

    suspend fun getDeal(dealId: String): Deal? {
        val response = apiService.retrieveDeal(dealId)
        return if (response.isSuccessful) {
            response.body()
        } else {
            null
        }
    }
}